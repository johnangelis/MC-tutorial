// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME HepMCDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "Classes.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *pairlEintcOHepMCcLcLGenParticlemUgR_Dictionary();
   static void pairlEintcOHepMCcLcLGenParticlemUgR_TClassManip(TClass*);
   static void *new_pairlEintcOHepMCcLcLGenParticlemUgR(void *p = 0);
   static void *newArray_pairlEintcOHepMCcLcLGenParticlemUgR(Long_t size, void *p);
   static void delete_pairlEintcOHepMCcLcLGenParticlemUgR(void *p);
   static void deleteArray_pairlEintcOHepMCcLcLGenParticlemUgR(void *p);
   static void destruct_pairlEintcOHepMCcLcLGenParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<int,HepMC::GenParticle*>*)
   {
      pair<int,HepMC::GenParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<int,HepMC::GenParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<int,HepMC::GenParticle*>", "string", 96,
                  typeid(pair<int,HepMC::GenParticle*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &pairlEintcOHepMCcLcLGenParticlemUgR_Dictionary, isa_proxy, 4,
                  sizeof(pair<int,HepMC::GenParticle*>) );
      instance.SetNew(&new_pairlEintcOHepMCcLcLGenParticlemUgR);
      instance.SetNewArray(&newArray_pairlEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDelete(&delete_pairlEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDeleteArray(&deleteArray_pairlEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDestructor(&destruct_pairlEintcOHepMCcLcLGenParticlemUgR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const pair<int,HepMC::GenParticle*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEintcOHepMCcLcLGenParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<int,HepMC::GenParticle*>*)0x0)->GetClass();
      pairlEintcOHepMCcLcLGenParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEintcOHepMCcLcLGenParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEintcOHepMCcLcLGenVertexmUgR_Dictionary();
   static void pairlEintcOHepMCcLcLGenVertexmUgR_TClassManip(TClass*);
   static void *new_pairlEintcOHepMCcLcLGenVertexmUgR(void *p = 0);
   static void *newArray_pairlEintcOHepMCcLcLGenVertexmUgR(Long_t size, void *p);
   static void delete_pairlEintcOHepMCcLcLGenVertexmUgR(void *p);
   static void deleteArray_pairlEintcOHepMCcLcLGenVertexmUgR(void *p);
   static void destruct_pairlEintcOHepMCcLcLGenVertexmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<int,HepMC::GenVertex*>*)
   {
      pair<int,HepMC::GenVertex*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<int,HepMC::GenVertex*>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<int,HepMC::GenVertex*>", "string", 96,
                  typeid(pair<int,HepMC::GenVertex*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &pairlEintcOHepMCcLcLGenVertexmUgR_Dictionary, isa_proxy, 4,
                  sizeof(pair<int,HepMC::GenVertex*>) );
      instance.SetNew(&new_pairlEintcOHepMCcLcLGenVertexmUgR);
      instance.SetNewArray(&newArray_pairlEintcOHepMCcLcLGenVertexmUgR);
      instance.SetDelete(&delete_pairlEintcOHepMCcLcLGenVertexmUgR);
      instance.SetDeleteArray(&deleteArray_pairlEintcOHepMCcLcLGenVertexmUgR);
      instance.SetDestructor(&destruct_pairlEintcOHepMCcLcLGenVertexmUgR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const pair<int,HepMC::GenVertex*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEintcOHepMCcLcLGenVertexmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<int,HepMC::GenVertex*>*)0x0)->GetClass();
      pairlEintcOHepMCcLcLGenVertexmUgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEintcOHepMCcLcLGenVertexmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLWeightContainer_Dictionary();
   static void HepMCcLcLWeightContainer_TClassManip(TClass*);
   static void *new_HepMCcLcLWeightContainer(void *p = 0);
   static void *newArray_HepMCcLcLWeightContainer(Long_t size, void *p);
   static void delete_HepMCcLcLWeightContainer(void *p);
   static void deleteArray_HepMCcLcLWeightContainer(void *p);
   static void destruct_HepMCcLcLWeightContainer(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::WeightContainer*)
   {
      ::HepMC::WeightContainer *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::WeightContainer));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::WeightContainer", "HepMC/GenEvent.h", 29,
                  typeid(::HepMC::WeightContainer), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLWeightContainer_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::WeightContainer) );
      instance.SetNew(&new_HepMCcLcLWeightContainer);
      instance.SetNewArray(&newArray_HepMCcLcLWeightContainer);
      instance.SetDelete(&delete_HepMCcLcLWeightContainer);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLWeightContainer);
      instance.SetDestructor(&destruct_HepMCcLcLWeightContainer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::WeightContainer*)
   {
      return GenerateInitInstanceLocal((::HepMC::WeightContainer*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::WeightContainer*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLWeightContainer_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::WeightContainer*)0x0)->GetClass();
      HepMCcLcLWeightContainer_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLWeightContainer_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLGenVertex_Dictionary();
   static void HepMCcLcLGenVertex_TClassManip(TClass*);
   static void *new_HepMCcLcLGenVertex(void *p = 0);
   static void *newArray_HepMCcLcLGenVertex(Long_t size, void *p);
   static void delete_HepMCcLcLGenVertex(void *p);
   static void deleteArray_HepMCcLcLGenVertex(void *p);
   static void destruct_HepMCcLcLGenVertex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::GenVertex*)
   {
      ::HepMC::GenVertex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::GenVertex));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::GenVertex", "HepMC/GenEvent.h", 52,
                  typeid(::HepMC::GenVertex), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLGenVertex_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::GenVertex) );
      instance.SetNew(&new_HepMCcLcLGenVertex);
      instance.SetNewArray(&newArray_HepMCcLcLGenVertex);
      instance.SetDelete(&delete_HepMCcLcLGenVertex);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLGenVertex);
      instance.SetDestructor(&destruct_HepMCcLcLGenVertex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::GenVertex*)
   {
      return GenerateInitInstanceLocal((::HepMC::GenVertex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::GenVertex*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLGenVertex_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::GenVertex*)0x0)->GetClass();
      HepMCcLcLGenVertex_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLGenVertex_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLFlow_Dictionary();
   static void HepMCcLcLFlow_TClassManip(TClass*);
   static void *new_HepMCcLcLFlow(void *p = 0);
   static void *newArray_HepMCcLcLFlow(Long_t size, void *p);
   static void delete_HepMCcLcLFlow(void *p);
   static void deleteArray_HepMCcLcLFlow(void *p);
   static void destruct_HepMCcLcLFlow(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::Flow*)
   {
      ::HepMC::Flow *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::Flow));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::Flow", "HepMC/GenEvent.h", 66,
                  typeid(::HepMC::Flow), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLFlow_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::Flow) );
      instance.SetNew(&new_HepMCcLcLFlow);
      instance.SetNewArray(&newArray_HepMCcLcLFlow);
      instance.SetDelete(&delete_HepMCcLcLFlow);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLFlow);
      instance.SetDestructor(&destruct_HepMCcLcLFlow);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::Flow*)
   {
      return GenerateInitInstanceLocal((::HepMC::Flow*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::Flow*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLFlow_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::Flow*)0x0)->GetClass();
      HepMCcLcLFlow_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLFlow_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLPolarization_Dictionary();
   static void HepMCcLcLPolarization_TClassManip(TClass*);
   static void *new_HepMCcLcLPolarization(void *p = 0);
   static void *newArray_HepMCcLcLPolarization(Long_t size, void *p);
   static void delete_HepMCcLcLPolarization(void *p);
   static void deleteArray_HepMCcLcLPolarization(void *p);
   static void destruct_HepMCcLcLPolarization(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::Polarization*)
   {
      ::HepMC::Polarization *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::Polarization));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::Polarization", "HepMC/GenEvent.h", 29,
                  typeid(::HepMC::Polarization), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLPolarization_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::Polarization) );
      instance.SetNew(&new_HepMCcLcLPolarization);
      instance.SetNewArray(&newArray_HepMCcLcLPolarization);
      instance.SetDelete(&delete_HepMCcLcLPolarization);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLPolarization);
      instance.SetDestructor(&destruct_HepMCcLcLPolarization);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::Polarization*)
   {
      return GenerateInitInstanceLocal((::HepMC::Polarization*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::Polarization*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLPolarization_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::Polarization*)0x0)->GetClass();
      HepMCcLcLPolarization_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLPolarization_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLGenParticle_Dictionary();
   static void HepMCcLcLGenParticle_TClassManip(TClass*);
   static void *new_HepMCcLcLGenParticle(void *p = 0);
   static void *newArray_HepMCcLcLGenParticle(Long_t size, void *p);
   static void delete_HepMCcLcLGenParticle(void *p);
   static void deleteArray_HepMCcLcLGenParticle(void *p);
   static void destruct_HepMCcLcLGenParticle(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::GenParticle*)
   {
      ::HepMC::GenParticle *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::GenParticle));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::GenParticle", "HepMC/GenEvent.h", 60,
                  typeid(::HepMC::GenParticle), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLGenParticle_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::GenParticle) );
      instance.SetNew(&new_HepMCcLcLGenParticle);
      instance.SetNewArray(&newArray_HepMCcLcLGenParticle);
      instance.SetDelete(&delete_HepMCcLcLGenParticle);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLGenParticle);
      instance.SetDestructor(&destruct_HepMCcLcLGenParticle);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::GenParticle*)
   {
      return GenerateInitInstanceLocal((::HepMC::GenParticle*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::GenParticle*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLGenParticle_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::GenParticle*)0x0)->GetClass();
      HepMCcLcLGenParticle_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLGenParticle_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLHeavyIon_Dictionary();
   static void HepMCcLcLHeavyIon_TClassManip(TClass*);
   static void *new_HepMCcLcLHeavyIon(void *p = 0);
   static void *newArray_HepMCcLcLHeavyIon(Long_t size, void *p);
   static void delete_HepMCcLcLHeavyIon(void *p);
   static void deleteArray_HepMCcLcLHeavyIon(void *p);
   static void destruct_HepMCcLcLHeavyIon(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::HeavyIon*)
   {
      ::HepMC::HeavyIon *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::HeavyIon));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::HeavyIon", "HepMC/GenEvent.h", 45,
                  typeid(::HepMC::HeavyIon), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLHeavyIon_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::HeavyIon) );
      instance.SetNew(&new_HepMCcLcLHeavyIon);
      instance.SetNewArray(&newArray_HepMCcLcLHeavyIon);
      instance.SetDelete(&delete_HepMCcLcLHeavyIon);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLHeavyIon);
      instance.SetDestructor(&destruct_HepMCcLcLHeavyIon);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::HeavyIon*)
   {
      return GenerateInitInstanceLocal((::HepMC::HeavyIon*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::HeavyIon*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLHeavyIon_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::HeavyIon*)0x0)->GetClass();
      HepMCcLcLHeavyIon_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLHeavyIon_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLPdfInfo_Dictionary();
   static void HepMCcLcLPdfInfo_TClassManip(TClass*);
   static void *new_HepMCcLcLPdfInfo(void *p = 0);
   static void *newArray_HepMCcLcLPdfInfo(Long_t size, void *p);
   static void delete_HepMCcLcLPdfInfo(void *p);
   static void deleteArray_HepMCcLcLPdfInfo(void *p);
   static void destruct_HepMCcLcLPdfInfo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::PdfInfo*)
   {
      ::HepMC::PdfInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::PdfInfo));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::PdfInfo", "HepMC/GenEvent.h", 37,
                  typeid(::HepMC::PdfInfo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLPdfInfo_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::PdfInfo) );
      instance.SetNew(&new_HepMCcLcLPdfInfo);
      instance.SetNewArray(&newArray_HepMCcLcLPdfInfo);
      instance.SetDelete(&delete_HepMCcLcLPdfInfo);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLPdfInfo);
      instance.SetDestructor(&destruct_HepMCcLcLPdfInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::PdfInfo*)
   {
      return GenerateInitInstanceLocal((::HepMC::PdfInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::PdfInfo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLPdfInfo_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::PdfInfo*)0x0)->GetClass();
      HepMCcLcLPdfInfo_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLPdfInfo_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HepMCcLcLGenEvent_Dictionary();
   static void HepMCcLcLGenEvent_TClassManip(TClass*);
   static void *new_HepMCcLcLGenEvent(void *p = 0);
   static void *newArray_HepMCcLcLGenEvent(Long_t size, void *p);
   static void delete_HepMCcLcLGenEvent(void *p);
   static void deleteArray_HepMCcLcLGenEvent(void *p);
   static void destruct_HepMCcLcLGenEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HepMC::GenEvent*)
   {
      ::HepMC::GenEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::HepMC::GenEvent));
      static ::ROOT::TGenericClassInfo 
         instance("HepMC::GenEvent", "HepMC/GenEvent.h", 155,
                  typeid(::HepMC::GenEvent), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HepMCcLcLGenEvent_Dictionary, isa_proxy, 4,
                  sizeof(::HepMC::GenEvent) );
      instance.SetNew(&new_HepMCcLcLGenEvent);
      instance.SetNewArray(&newArray_HepMCcLcLGenEvent);
      instance.SetDelete(&delete_HepMCcLcLGenEvent);
      instance.SetDeleteArray(&deleteArray_HepMCcLcLGenEvent);
      instance.SetDestructor(&destruct_HepMCcLcLGenEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HepMC::GenEvent*)
   {
      return GenerateInitInstanceLocal((::HepMC::GenEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HepMC::GenEvent*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HepMCcLcLGenEvent_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::HepMC::GenEvent*)0x0)->GetClass();
      HepMCcLcLGenEvent_TClassManip(theClass);
   return theClass;
   }

   static void HepMCcLcLGenEvent_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEintcOHepMCcLcLGenParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) pair<int,HepMC::GenParticle*> : new pair<int,HepMC::GenParticle*>;
   }
   static void *newArray_pairlEintcOHepMCcLcLGenParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) pair<int,HepMC::GenParticle*>[nElements] : new pair<int,HepMC::GenParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEintcOHepMCcLcLGenParticlemUgR(void *p) {
      delete ((pair<int,HepMC::GenParticle*>*)p);
   }
   static void deleteArray_pairlEintcOHepMCcLcLGenParticlemUgR(void *p) {
      delete [] ((pair<int,HepMC::GenParticle*>*)p);
   }
   static void destruct_pairlEintcOHepMCcLcLGenParticlemUgR(void *p) {
      typedef pair<int,HepMC::GenParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<int,HepMC::GenParticle*>

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEintcOHepMCcLcLGenVertexmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) pair<int,HepMC::GenVertex*> : new pair<int,HepMC::GenVertex*>;
   }
   static void *newArray_pairlEintcOHepMCcLcLGenVertexmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) pair<int,HepMC::GenVertex*>[nElements] : new pair<int,HepMC::GenVertex*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEintcOHepMCcLcLGenVertexmUgR(void *p) {
      delete ((pair<int,HepMC::GenVertex*>*)p);
   }
   static void deleteArray_pairlEintcOHepMCcLcLGenVertexmUgR(void *p) {
      delete [] ((pair<int,HepMC::GenVertex*>*)p);
   }
   static void destruct_pairlEintcOHepMCcLcLGenVertexmUgR(void *p) {
      typedef pair<int,HepMC::GenVertex*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<int,HepMC::GenVertex*>

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLWeightContainer(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::WeightContainer : new ::HepMC::WeightContainer;
   }
   static void *newArray_HepMCcLcLWeightContainer(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::WeightContainer[nElements] : new ::HepMC::WeightContainer[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLWeightContainer(void *p) {
      delete ((::HepMC::WeightContainer*)p);
   }
   static void deleteArray_HepMCcLcLWeightContainer(void *p) {
      delete [] ((::HepMC::WeightContainer*)p);
   }
   static void destruct_HepMCcLcLWeightContainer(void *p) {
      typedef ::HepMC::WeightContainer current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::WeightContainer

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLGenVertex(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenVertex : new ::HepMC::GenVertex;
   }
   static void *newArray_HepMCcLcLGenVertex(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenVertex[nElements] : new ::HepMC::GenVertex[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLGenVertex(void *p) {
      delete ((::HepMC::GenVertex*)p);
   }
   static void deleteArray_HepMCcLcLGenVertex(void *p) {
      delete [] ((::HepMC::GenVertex*)p);
   }
   static void destruct_HepMCcLcLGenVertex(void *p) {
      typedef ::HepMC::GenVertex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::GenVertex

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLFlow(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::Flow : new ::HepMC::Flow;
   }
   static void *newArray_HepMCcLcLFlow(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::Flow[nElements] : new ::HepMC::Flow[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLFlow(void *p) {
      delete ((::HepMC::Flow*)p);
   }
   static void deleteArray_HepMCcLcLFlow(void *p) {
      delete [] ((::HepMC::Flow*)p);
   }
   static void destruct_HepMCcLcLFlow(void *p) {
      typedef ::HepMC::Flow current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::Flow

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLPolarization(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::Polarization : new ::HepMC::Polarization;
   }
   static void *newArray_HepMCcLcLPolarization(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::Polarization[nElements] : new ::HepMC::Polarization[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLPolarization(void *p) {
      delete ((::HepMC::Polarization*)p);
   }
   static void deleteArray_HepMCcLcLPolarization(void *p) {
      delete [] ((::HepMC::Polarization*)p);
   }
   static void destruct_HepMCcLcLPolarization(void *p) {
      typedef ::HepMC::Polarization current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::Polarization

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLGenParticle(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenParticle : new ::HepMC::GenParticle;
   }
   static void *newArray_HepMCcLcLGenParticle(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenParticle[nElements] : new ::HepMC::GenParticle[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLGenParticle(void *p) {
      delete ((::HepMC::GenParticle*)p);
   }
   static void deleteArray_HepMCcLcLGenParticle(void *p) {
      delete [] ((::HepMC::GenParticle*)p);
   }
   static void destruct_HepMCcLcLGenParticle(void *p) {
      typedef ::HepMC::GenParticle current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::GenParticle

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLHeavyIon(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::HeavyIon : new ::HepMC::HeavyIon;
   }
   static void *newArray_HepMCcLcLHeavyIon(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::HeavyIon[nElements] : new ::HepMC::HeavyIon[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLHeavyIon(void *p) {
      delete ((::HepMC::HeavyIon*)p);
   }
   static void deleteArray_HepMCcLcLHeavyIon(void *p) {
      delete [] ((::HepMC::HeavyIon*)p);
   }
   static void destruct_HepMCcLcLHeavyIon(void *p) {
      typedef ::HepMC::HeavyIon current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::HeavyIon

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLPdfInfo(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::PdfInfo : new ::HepMC::PdfInfo;
   }
   static void *newArray_HepMCcLcLPdfInfo(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::PdfInfo[nElements] : new ::HepMC::PdfInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLPdfInfo(void *p) {
      delete ((::HepMC::PdfInfo*)p);
   }
   static void deleteArray_HepMCcLcLPdfInfo(void *p) {
      delete [] ((::HepMC::PdfInfo*)p);
   }
   static void destruct_HepMCcLcLPdfInfo(void *p) {
      typedef ::HepMC::PdfInfo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::PdfInfo

namespace ROOT {
   // Wrappers around operator new
   static void *new_HepMCcLcLGenEvent(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenEvent : new ::HepMC::GenEvent;
   }
   static void *newArray_HepMCcLcLGenEvent(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::HepMC::GenEvent[nElements] : new ::HepMC::GenEvent[nElements];
   }
   // Wrapper around operator delete
   static void delete_HepMCcLcLGenEvent(void *p) {
      delete ((::HepMC::GenEvent*)p);
   }
   static void deleteArray_HepMCcLcLGenEvent(void *p) {
      delete [] ((::HepMC::GenEvent*)p);
   }
   static void destruct_HepMCcLcLGenEvent(void *p) {
      typedef ::HepMC::GenEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HepMC::GenEvent

namespace ROOT {
   static TClass *vectorlElonggR_Dictionary();
   static void vectorlElonggR_TClassManip(TClass*);
   static void *new_vectorlElonggR(void *p = 0);
   static void *newArray_vectorlElonggR(Long_t size, void *p);
   static void delete_vectorlElonggR(void *p);
   static void deleteArray_vectorlElonggR(void *p);
   static void destruct_vectorlElonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<long>*)
   {
      vector<long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<long>", -2, "vector", 214,
                  typeid(vector<long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlElonggR_Dictionary, isa_proxy, 0,
                  sizeof(vector<long>) );
      instance.SetNew(&new_vectorlElonggR);
      instance.SetNewArray(&newArray_vectorlElonggR);
      instance.SetDelete(&delete_vectorlElonggR);
      instance.SetDeleteArray(&deleteArray_vectorlElonggR);
      instance.SetDestructor(&destruct_vectorlElonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<long>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlElonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<long>*)0x0)->GetClass();
      vectorlElonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlElonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlElonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long> : new vector<long>;
   }
   static void *newArray_vectorlElonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<long>[nElements] : new vector<long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlElonggR(void *p) {
      delete ((vector<long>*)p);
   }
   static void deleteArray_vectorlElonggR(void *p) {
      delete [] ((vector<long>*)p);
   }
   static void destruct_vectorlElonggR(void *p) {
      typedef vector<long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<long>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlEHepMCcLcLGenParticlemUgR_Dictionary();
   static void vectorlEHepMCcLcLGenParticlemUgR_TClassManip(TClass*);
   static void *new_vectorlEHepMCcLcLGenParticlemUgR(void *p = 0);
   static void *newArray_vectorlEHepMCcLcLGenParticlemUgR(Long_t size, void *p);
   static void delete_vectorlEHepMCcLcLGenParticlemUgR(void *p);
   static void deleteArray_vectorlEHepMCcLcLGenParticlemUgR(void *p);
   static void destruct_vectorlEHepMCcLcLGenParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<HepMC::GenParticle*>*)
   {
      vector<HepMC::GenParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<HepMC::GenParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<HepMC::GenParticle*>", -2, "vector", 214,
                  typeid(vector<HepMC::GenParticle*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHepMCcLcLGenParticlemUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<HepMC::GenParticle*>) );
      instance.SetNew(&new_vectorlEHepMCcLcLGenParticlemUgR);
      instance.SetNewArray(&newArray_vectorlEHepMCcLcLGenParticlemUgR);
      instance.SetDelete(&delete_vectorlEHepMCcLcLGenParticlemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHepMCcLcLGenParticlemUgR);
      instance.SetDestructor(&destruct_vectorlEHepMCcLcLGenParticlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<HepMC::GenParticle*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<HepMC::GenParticle*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHepMCcLcLGenParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<HepMC::GenParticle*>*)0x0)->GetClass();
      vectorlEHepMCcLcLGenParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHepMCcLcLGenParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHepMCcLcLGenParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<HepMC::GenParticle*> : new vector<HepMC::GenParticle*>;
   }
   static void *newArray_vectorlEHepMCcLcLGenParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<HepMC::GenParticle*>[nElements] : new vector<HepMC::GenParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHepMCcLcLGenParticlemUgR(void *p) {
      delete ((vector<HepMC::GenParticle*>*)p);
   }
   static void deleteArray_vectorlEHepMCcLcLGenParticlemUgR(void *p) {
      delete [] ((vector<HepMC::GenParticle*>*)p);
   }
   static void destruct_vectorlEHepMCcLcLGenParticlemUgR(void *p) {
      typedef vector<HepMC::GenParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<HepMC::GenParticle*>

namespace ROOT {
   static TClass *maplEstringcOunsignedsPlonggR_Dictionary();
   static void maplEstringcOunsignedsPlonggR_TClassManip(TClass*);
   static void *new_maplEstringcOunsignedsPlonggR(void *p = 0);
   static void *newArray_maplEstringcOunsignedsPlonggR(Long_t size, void *p);
   static void delete_maplEstringcOunsignedsPlonggR(void *p);
   static void deleteArray_maplEstringcOunsignedsPlonggR(void *p);
   static void destruct_maplEstringcOunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,unsigned long>*)
   {
      map<string,unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,unsigned long>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,unsigned long>", -2, "map", 96,
                  typeid(map<string,unsigned long>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOunsignedsPlonggR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,unsigned long>) );
      instance.SetNew(&new_maplEstringcOunsignedsPlonggR);
      instance.SetNewArray(&newArray_maplEstringcOunsignedsPlonggR);
      instance.SetDelete(&delete_maplEstringcOunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOunsignedsPlonggR);
      instance.SetDestructor(&destruct_maplEstringcOunsignedsPlonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,unsigned long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,unsigned long>*)0x0)->GetClass();
      maplEstringcOunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOunsignedsPlonggR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,unsigned long> : new map<string,unsigned long>;
   }
   static void *newArray_maplEstringcOunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,unsigned long>[nElements] : new map<string,unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOunsignedsPlonggR(void *p) {
      delete ((map<string,unsigned long>*)p);
   }
   static void deleteArray_maplEstringcOunsignedsPlonggR(void *p) {
      delete [] ((map<string,unsigned long>*)p);
   }
   static void destruct_maplEstringcOunsignedsPlonggR(void *p) {
      typedef map<string,unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,unsigned long>

namespace ROOT {
   static TClass *maplEintcOintgR_Dictionary();
   static void maplEintcOintgR_TClassManip(TClass*);
   static void *new_maplEintcOintgR(void *p = 0);
   static void *newArray_maplEintcOintgR(Long_t size, void *p);
   static void delete_maplEintcOintgR(void *p);
   static void deleteArray_maplEintcOintgR(void *p);
   static void destruct_maplEintcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,int>*)
   {
      map<int,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,int>", -2, "map", 96,
                  typeid(map<int,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,int>) );
      instance.SetNew(&new_maplEintcOintgR);
      instance.SetNewArray(&newArray_maplEintcOintgR);
      instance.SetDelete(&delete_maplEintcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOintgR);
      instance.SetDestructor(&destruct_maplEintcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,int>*)0x0)->GetClass();
      maplEintcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int> : new map<int,int>;
   }
   static void *newArray_maplEintcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int>[nElements] : new map<int,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOintgR(void *p) {
      delete ((map<int,int>*)p);
   }
   static void deleteArray_maplEintcOintgR(void *p) {
      delete [] ((map<int,int>*)p);
   }
   static void destruct_maplEintcOintgR(void *p) {
      typedef map<int,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,int>

namespace ROOT {
   static TClass *maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_Dictionary();
   static void maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_TClassManip(TClass*);
   static void *new_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p = 0);
   static void *newArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(Long_t size, void *p);
   static void delete_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p);
   static void deleteArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p);
   static void destruct_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,HepMC::GenVertex*,greater<int> >*)
   {
      map<int,HepMC::GenVertex*,greater<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,HepMC::GenVertex*,greater<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,HepMC::GenVertex*,greater<int> >", -2, "map", 96,
                  typeid(map<int,HepMC::GenVertex*,greater<int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<int,HepMC::GenVertex*,greater<int> >) );
      instance.SetNew(&new_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR);
      instance.SetNewArray(&newArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR);
      instance.SetDelete(&delete_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR);
      instance.SetDestructor(&destruct_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,HepMC::GenVertex*,greater<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,HepMC::GenVertex*,greater<int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,HepMC::GenVertex*,greater<int> >*)0x0)->GetClass();
      maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,HepMC::GenVertex*,greater<int> > : new map<int,HepMC::GenVertex*,greater<int> >;
   }
   static void *newArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,HepMC::GenVertex*,greater<int> >[nElements] : new map<int,HepMC::GenVertex*,greater<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p) {
      delete ((map<int,HepMC::GenVertex*,greater<int> >*)p);
   }
   static void deleteArray_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p) {
      delete [] ((map<int,HepMC::GenVertex*,greater<int> >*)p);
   }
   static void destruct_maplEintcOHepMCcLcLGenVertexmUcOgreaterlEintgRsPgR(void *p) {
      typedef map<int,HepMC::GenVertex*,greater<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,HepMC::GenVertex*,greater<int> >

namespace ROOT {
   static TClass *maplEintcOHepMCcLcLGenParticlemUgR_Dictionary();
   static void maplEintcOHepMCcLcLGenParticlemUgR_TClassManip(TClass*);
   static void *new_maplEintcOHepMCcLcLGenParticlemUgR(void *p = 0);
   static void *newArray_maplEintcOHepMCcLcLGenParticlemUgR(Long_t size, void *p);
   static void delete_maplEintcOHepMCcLcLGenParticlemUgR(void *p);
   static void deleteArray_maplEintcOHepMCcLcLGenParticlemUgR(void *p);
   static void destruct_maplEintcOHepMCcLcLGenParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,HepMC::GenParticle*>*)
   {
      map<int,HepMC::GenParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,HepMC::GenParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,HepMC::GenParticle*>", -2, "map", 96,
                  typeid(map<int,HepMC::GenParticle*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOHepMCcLcLGenParticlemUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<int,HepMC::GenParticle*>) );
      instance.SetNew(&new_maplEintcOHepMCcLcLGenParticlemUgR);
      instance.SetNewArray(&newArray_maplEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDelete(&delete_maplEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOHepMCcLcLGenParticlemUgR);
      instance.SetDestructor(&destruct_maplEintcOHepMCcLcLGenParticlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,HepMC::GenParticle*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,HepMC::GenParticle*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOHepMCcLcLGenParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,HepMC::GenParticle*>*)0x0)->GetClass();
      maplEintcOHepMCcLcLGenParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOHepMCcLcLGenParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOHepMCcLcLGenParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,HepMC::GenParticle*> : new map<int,HepMC::GenParticle*>;
   }
   static void *newArray_maplEintcOHepMCcLcLGenParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,HepMC::GenParticle*>[nElements] : new map<int,HepMC::GenParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOHepMCcLcLGenParticlemUgR(void *p) {
      delete ((map<int,HepMC::GenParticle*>*)p);
   }
   static void deleteArray_maplEintcOHepMCcLcLGenParticlemUgR(void *p) {
      delete [] ((map<int,HepMC::GenParticle*>*)p);
   }
   static void destruct_maplEintcOHepMCcLcLGenParticlemUgR(void *p) {
      typedef map<int,HepMC::GenParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,HepMC::GenParticle*>

namespace {
  void TriggerDictionaryInitialization_HepMCDict_Impl() {
    static const char* headers[] = {
"Classes.h",
0
    };
    static const char* includePaths[] = {
"/home/john/Programs/hepmc2/include",
"/home/john/Programs/root/include",
"/home/john/Programs/root/include",
"/home/john/Desktop/Untitled Folder/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HepMCDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/GenParticle.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  GenParticle;}
namespace std{template <class _T1, class _T2> struct __attribute__((annotate("$clingAutoload$bits/stl_pair.h")))  __attribute__((annotate("$clingAutoload$string")))  pair;
}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/GenVertex.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  GenVertex;}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$bits/stl_function.h")))  __attribute__((annotate("$clingAutoload$string")))  greater;
}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/WeightContainer.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  WeightContainer;}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/Flow.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  Flow;}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/Polarization.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  Polarization;}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/HeavyIon.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  HeavyIon;}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/PdfInfo.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  PdfInfo;}
namespace HepMC{class __attribute__((annotate("$clingAutoload$HepMC/GenEvent.h")))  __attribute__((annotate("$clingAutoload$Classes.h")))  GenEvent;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HepMCDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "Classes.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"HepMC::Flow", payloadCode, "@",
"HepMC::GenEvent", payloadCode, "@",
"HepMC::GenParticle", payloadCode, "@",
"HepMC::GenVertex", payloadCode, "@",
"HepMC::HeavyIon", payloadCode, "@",
"HepMC::PdfInfo", payloadCode, "@",
"HepMC::Polarization", payloadCode, "@",
"HepMC::WeightContainer", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HepMCDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HepMCDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HepMCDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HepMCDict() {
  TriggerDictionaryInitialization_HepMCDict_Impl();
}
